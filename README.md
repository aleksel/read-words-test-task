# Test task:

## Run:

* The program might be compiled as jar file by command **"gradlew clean jar"**
* Execution command is **"java jar read-words-test-task.jar"** will show help

## Assumptions:

* Files must be placed into the same directory when the application.
* From the condition we know that file might contain maximum 500 words, so both files read entirely into memory.  
* We consider characters as a word if the first character is alphabetical.
* Words will be transformed to lower case, it means we count words without thinking about characters register.
* Words must contain only alphabetical symbols, other characters will be removed.

## Condition:

### Write a multi-threaded application in Java 7 or 8.

You�re given 2 text files on local filesystem, each contains a piece of
text in English, maximum 500 words long. The files are encoded in
UTF-8.

Two threads simultaneously read each its own file and populate a
shared cache. When they finish, a separate thread traverses the cache
and prints out the following stats for each word, one line per word:

__<word> <total occurrences> = <occurrences in file1> + <occurrences in file2>__

where:

* <word> - a word that has been seen in at least one of the files
* <total occurrences> - a number of times this word has been
seen in both files together
* <occurrences in file#> - a number of times this word has been
seen in the respective file

The output shall be order descending by <total occurrences>, starting from the biggest value.

---

You are free to give your interpretation to any aspect that�s not
explicitly detailed above. These assumptions shall be detailed together
with the implementation.