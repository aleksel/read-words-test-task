package com.company;

import com.company.task.MainTask;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if (args.length == 0 || (args.length == 1 && (args[0].equals("--help") || args[0].equals("-h")))) {
            System.out.println();
            System.out.println("  Usage: java -jar read-words-test-task.jar [first file name] " +
                    "[second file name] [options]");
            System.out.println("    *The files must be placed into the same directory with jar file");
            System.out.println();
            System.out.println("  Options:");
            System.out.println();
            System.out.printf("    %-50s %s",
                    "-f=FORMAT", "Default result output is into console, available options are: json, csv, console");
            System.out.println();
            System.out.printf("    %-50s %s",
                    "-l=LOGGER_LEVEL", "By default logger is off, available options are: all, trace, debug, " +
                            "info, warn, error, fatal, off");
            System.out.println();
            System.exit(0);
        }
        if (args.length < 2) {
            System.out.println("ERROR: You must specify exactly 2 files into the same directory with this program " +
                    "as arguments, received: " + args.length);
            System.exit(-1);
        }

        String format = parseOptionalArguments(args);
        String firstFileName = System.getProperty("user.dir") + "/" + args[0];
        checkArguments(firstFileName);
        String secondFileName = System.getProperty("user.dir") + "/" + args[1];
        checkArguments(secondFileName);
        MainTask mainTask = new MainTask();
        try {
            long startTime = System.currentTimeMillis();

            mainTask.execute(firstFileName, secondFileName, format);

            if (LOGGER.isDebugEnabled()) LOGGER.debug("Execution time is: {}", System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            System.out.println("ERROR");
            if (e.getCause() != null) System.out.println("CAUSE: " + e.getCause().getMessage());
            System.out.println("CAUSE: {}" + e.getMessage());
            LOGGER.error("ERROR", e);
        }
    }

    private static void checkArguments(String fileName) {
        Path path = null;
        try {
            path = Paths.get(fileName);
        } catch (Exception e) {
            System.out.println("ERROR: file name: \"\" + fileName + \"\" is incorrect");
            System.exit(-1);
        }

        if (Files.notExists(path)) {
            System.out.println("ERROR: file: \"\" + fileName + \"\" not exist");
            System.exit(-1);
        }

        if (!Files.isRegularFile(path)) {
            System.out.println("ERROR: file: \"" + fileName + "\" isn't regular text file");
            System.exit(-1);
        }

        if (!Files.isReadable(path)) {
            System.out.println("ERROR: file: \"\" + fileName + \"\" isn't readable, perhaps you don't have enough permissions");
            System.exit(-1);
        }
    }

    private static String parseOptionalArguments(String[] args) {
        if (args.length < 3) return null;
        String format = null;
        for (int i = 2; i < args.length; i++) {
            String arg = args[i];
            String[] split = arg.split("=");
            if (split.length == 2) {
                switch (split[0].toLowerCase()) {
                    case "-f": {
                        format = split[1].toLowerCase();
                        if (!format.equals("csv") && !format.equals("json") && !format.equals("console")) {
                            System.out.println("ERROR: Option \"-f\" can be only \"csv\", \"json\" or " +
                                    "\"console\", received: " + format);
                            System.exit(-1);
                        }
                        break;
                    }
                    case "-l": {
                        try {
                            Configurator.setRootLevel(Level.getLevel(split[1].toUpperCase()));
                        } catch (Exception e) {
                            System.out.println("ERROR: Option \"-l\" must be valid logger level \"debug\", " +
                                    "\"info\", \"error\" etc, received: " + split[1]);
                            System.exit(-1);
                        }
                        break;
                    }
                    default:
                        System.out.println("ERROR: Option \"" + arg + "\" is undefined");
                        System.exit(-1);
                }
            } else {
                System.out.println("ERROR: Option \"" + arg + "\" is undefined");
                System.exit(-1);
            }
        }
        return format;
    }
}