package com.company.task;

import com.company.entity.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MainTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainTask.class);
    private static final Function<Result, String> csvMapper = result ->
            "\"" + result.getWord() + "\",\""
                    + result.getCountInBothFiles() + "\",\""
                    + result.getCountInFirstFile() + "\",\""
                    + result.getCountInSecondFile() + "\"";

    private static List<Result> mergeFilesResult(List<Map<String, Integer>> textMap) {
        if (textMap == null || textMap.size() != 2) {
            throw new IllegalArgumentException("Input list must contain exactly two maps");
        }
        long startTime = System.currentTimeMillis();
        Map<String, Integer> firstFile = textMap.get(0);
        List<Result> resultList = new ArrayList<>();
        textMap.get(1).forEach((word, count) ->
                resultList.add(new Result(word, firstFile.getOrDefault(word, 0), count)));

        resultList.sort((result1, result2) ->
                Integer.compare(result2.getCountInBothFiles(), result1.getCountInBothFiles()));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Merged words: {}, unique words: {}, took: {}",
                    resultList.stream().map(Result::getCountInBothFiles).reduce(0, (a, b) -> a + b),
                    resultList.size(), System.currentTimeMillis() - startTime);
            LOGGER.debug("");
        }

        return resultList;
    }

    private static void outputResult(List<Result> resultList, String format) throws IOException {
        if (resultList == null) {
            throw new IllegalArgumentException("Input list must not be null");
        }
        long startTime = System.currentTimeMillis();
        System.out.println();
        System.out.printf("%42s", "FINISHED SUCCESSFULLY");
        System.out.println();
        System.out.println(String.join("", Collections.nCopies(65, "=")));
        System.out.println();

        if (format == null || format.equals("console")) {
            resultList.forEach(result -> System.out.printf("%-50s%-3d = %-3d + %-3d%n",
                    result.getWord(),
                    result.getCountInBothFiles(),
                    result.getCountInFirstFile(),
                    result.getCountInSecondFile())
            );
        } else if (format.equals("json")) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.writeValue(Paths.get(System.getProperty("user.dir") + "/result.json").toFile(), resultList);
            } catch (IOException e) {
                LOGGER.error("Error occurred when trying write data into file as json", e);
                throw e;
            }
        } else if (format.equals("csv")) {
            List<String> stringList = resultList.stream().map(csvMapper).collect(Collectors.toList());
            Files.write(Paths.get(System.getProperty("user.dir") + "/result.csv"),
                    stringList, Charset.forName("UTF8"));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Output the result took: {}", System.currentTimeMillis() - startTime);
            LOGGER.debug("");
        }
    }

    public void execute(String firstFileName, String secondFileName, String format)
            throws InterruptedException, ExecutionException, IOException {
        if (Strings.isBlank(firstFileName) || Strings.isBlank(secondFileName)) {
            throw new IllegalArgumentException("Must be specify exactly 2 file names");
        }
        List<Map<String, Integer>> textMap = new ArrayList<>(2);
        ExecutorService service = Executors.newFixedThreadPool(2);
        List<Future<Map<String, Integer>>> futures;
        try {
            futures = service.invokeAll(new ArrayList<FileReaderTask>(2) {{
                add(new FileReaderTask(firstFileName));
                add(new FileReaderTask(secondFileName));
            }});
        } catch (InterruptedException e) {
            LOGGER.error("Error occurred when trying to read the files", e);
            throw e;
        }
        for (Future<Map<String, Integer>> future : futures) {
            try {
                textMap.add(future.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Error occurred when trying to read the files", e);
                throw e;
            }
        }
        service.shutdown();
        List<Result> resultList = mergeFilesResult(textMap);
        outputResult(resultList, format);
    }
}
