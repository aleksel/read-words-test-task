package com.company.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class FileReaderTask implements Callable<Map<String, Integer>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileReaderTask.class);
    private final String filePath;
    private final Pattern splitPattern = Pattern.compile("\\s");

    public FileReaderTask(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public Map<String, Integer> call() throws IOException {
        long startTime = System.currentTimeMillis();
        Thread.currentThread().setName("FileReaderTask-" + Thread.currentThread().getId());
        Map<String, Integer> result = new HashMap<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(filePath), Charset.forName("UTF8"))) {
            bufferedReader.lines()
                    .flatMap(splitPattern::splitAsStream)
                    .filter(word -> word.length() > 0 && Character.isLetter(word.charAt(0)))
                    .map(word -> word.toLowerCase().replaceAll("[^a-z]", ""))
                    .forEach(word ->
                            result.compute(word, (wordKey, counter) -> counter == null ? 1 : ++counter));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Read words: {}, unique words: {}, from file: {}, took: {}",
                    result.values().stream().reduce(0, (a, b) -> a + b),
                    result.size(), filePath, System.currentTimeMillis() - startTime);
            LOGGER.debug("");
        }

        return result;
    }
}
