package com.company.entity;

public class Result {
    private String word;
    private int countInFirstFile;
    private int countInSecondFile;
    private int countInBothFiles;

    public Result(String word, int countInFirstFile, int countInSecondFile) {
        this.word = word;
        this.countInFirstFile = countInFirstFile;
        this.countInSecondFile = countInSecondFile;
        this.countInBothFiles = countInFirstFile + countInSecondFile;
    }

    public Result() {
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCountInFirstFile() {
        return countInFirstFile;
    }

    public void setCountInFirstFile(int countInFirstFile) {
        this.countInFirstFile = countInFirstFile;
    }

    public int getCountInSecondFile() {
        return countInSecondFile;
    }

    public void setCountInSecondFile(int countInSecondFile) {
        this.countInSecondFile = countInSecondFile;
    }

    public int getCountInBothFiles() {
        return countInBothFiles;
    }

    public void setCountInBothFiles(int countInBothFiles) {
        this.countInBothFiles = countInBothFiles;
    }
}
